package cz.cvut.fel.ts1.hw04.archive;

import cz.cvut.fel.ts1.hw04.shop.Item;
import cz.cvut.fel.ts1.hw04.shop.Order;
import cz.cvut.fel.ts1.hw04.shop.ShoppingCart;
import cz.cvut.fel.ts1.hw04.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class PurchasesArchiveTest {

    private ItemPurchaseArchiveEntry itemPurchaseArchiveEntry;
    private ArrayList mockedOrderArchive;
    private ArrayList<Order> orderArchive;
    private PurchasesArchive purchasesArchive;
    private HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive;
    private ByteArrayOutputStream outputStreamCaptor;
    String printedOutput;


    private StandardItem initItem(int id, String name, float price, String category, int loyaltyPoints){
        return new StandardItem( id, name, price, category, loyaltyPoints);
    }

    @BeforeEach
    public void redirectOut_capturesStdOut_StreamRedirected(){
        outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    public void createArchive_mockOrderArchive_newArchiveCreated(){
        StandardItem item = initItem(1, "Item1", 10.0f, "Category1", 10);
        mockedOrderArchive = Mockito.mock(ArrayList.class);
        itemPurchaseArchive = new HashMap<>();
        itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(item);
    }

    @Test
    @DisplayName("Test Statistic String Output:")
    public void printItemPurchaseStatistics_createsArchiveWithItemEntry_printsCapturedStatisticString() {
        createArchive_mockOrderArchive_newArchiveCreated();
        itemPurchaseArchive.put(5, itemPurchaseArchiveEntry);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchive, mockedOrderArchive);

        String expectedMessage =
                "ITEM PURCHASE STATISTICS:" + System.lineSeparator() +
                        itemPurchaseArchiveEntry.toString();

        purchasesArchive.printItemPurchaseStatistics();
        System.setOut(System.out);
        printedOutput = outputStreamCaptor.toString().trim();
        assertEquals(expectedMessage, printedOutput);
    }

    @Test
    @DisplayName("Test increase in how many times has been item sold:")
    public void getHowManyTimesHasBeenItemSold_increasesCountOfTimesSold_printsCurrentValueOfTimesSold() {
        createArchive_mockOrderArchive_newArchiveCreated();
        int currentTimesSold = itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold();
        itemPurchaseArchiveEntry.increaseCountHowManyTimesHasBeenSold(10);
        int timesSold = itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold();
        assertEquals(currentTimesSold + 10, timesSold);
    }

    @Test
    @DisplayName("Test Constructor of ItemPurchaseArchiveEntry:")
    public void putOrderToPurchasesArchive_createsNewArchiveAndMockedItemEntryConstructor_constructorCalled() {
        Item item1 = initItem(2, "Item2", 20.0f, "Category2", 20);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item1);
        ShoppingCart shoppingCart = new ShoppingCart(items);

        Order order = new Order(shoppingCart, "Som Ting Wong", "BehindYou");
        ItemPurchaseArchiveEntry mockeditemEntry = Mockito.mock(ItemPurchaseArchiveEntry.class);
        itemPurchaseArchive = new HashMap<>();
        itemPurchaseArchive.put(1, mockeditemEntry);

        orderArchive = Mockito.mock(ArrayList.class);

        try (MockedConstruction <ItemPurchaseArchiveEntry> mockedConstruction = Mockito.mockConstruction(ItemPurchaseArchiveEntry.class)) {
            purchasesArchive = new PurchasesArchive(itemPurchaseArchive, orderArchive);
            purchasesArchive.putOrderToPurchasesArchive(order);
            Mockito.verify(orderArchive, times(1)).add(order);
            assertEquals(1, mockedConstruction.constructed().size());
        }
    }
}



