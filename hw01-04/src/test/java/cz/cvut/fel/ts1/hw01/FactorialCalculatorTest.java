package cz.cvut.fel.ts1.hw01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FactorialCalculatorTest {

    @Test
    void factorialTest() {
        FactorialCalculator factorialCalculator = new FactorialCalculator();
        int number = 3;
        long expectedResult = 6;

        long result = factorialCalculator.factorial(number);

        Assertions.assertEquals(expectedResult, result);
    }
}