package cz.cvut.fel.ts1.hw04.storage;

import cz.cvut.fel.ts1.hw04.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    private StandardItem item1;
    private ItemStock itemStock;

    private void initItem(
            int id, String name, float price, String category, int loyaltyPoints){
        item1 = new StandardItem(id, name, price, category, loyaltyPoints);
        itemStock = new ItemStock(item1);
    }

    @Test
    @DisplayName("Test Constructor:")
    public void testConstructor_compareItemAttributes_attributesMatch (){
         initItem(1, "Item", 10.0f, "Category", 10);

//         testing the Item
         assertEquals(item1, itemStock.getItem());

//         testing count
        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @DisplayName("Test increase in ItemStock:")
    @CsvSource({
            "5", "6", "7", "10"
    })
    void increaseItemCount_increaseNumberOfItems_numberOfItemsInStock (
            int numberOfItems) {
        initItem(1, "Item1", 10.0f, "Category1", 10);

        itemStock.IncreaseItemCount(numberOfItems);
        assertEquals(numberOfItems, itemStock.getCount());
    }

    @ParameterizedTest
    @DisplayName("Test decrease in ItemStock:")
    @CsvSource({
            "5, 3", "7, 6", "9, 9"
    })
    void decreaseItemCount_increaseByFirstNumberAndDecreaseBySecondNumber_NumberOfItemsInStock (
            int itemsInStock, int itemsSold) {
        initItem(1, "Item1", 10.0f, "Category1", 10);

        itemStock.IncreaseItemCount(itemsInStock);
        itemStock.decreaseItemCount(itemsSold);
        assertEquals(itemsInStock - itemsSold, itemStock.getCount());
    }
}