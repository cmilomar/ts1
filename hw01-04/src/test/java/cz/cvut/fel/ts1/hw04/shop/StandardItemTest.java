package cz.cvut.fel.ts1.hw04.shop;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {

    @ParameterizedTest
    @DisplayName("Test Constructor:")
    @CsvSource({
            "1, Item1, 10.0f, Category1, 10",
            "2, Item2, 20.0f, Category2, 20",
            "3, Item3, 30.0f, Category3, 30"
    })

    public void testConstructor_compareItemAttributes_attributesMatch(
            int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        assertNotNull(standardItem);
        assertEquals(id, standardItem.getID());
        assertEquals(name, standardItem.getName());
        assertEquals(price, standardItem.getPrice());
        assertEquals(category, standardItem.getCategory());
        assertEquals(loyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @DisplayName("Parameterized test Equals:")
    @CsvSource({
            "1, Item1, 10.0f, Category1, 10, true",
            "2, Item1, 10.0f, Category1, 10, false",
            "1, Item2, 10.0f, Category1, 10, false",
            "1, Item1, 20.0f, Category1, 10, false",
            "1, Item1, 10.0f, Category2, 10, false",
            "1, Item1, 10.0f, Category1, 20, false"
    })

    public void testEquals_compareItemsEquality_Boolean(
            int id, String name, float price, String category, int loyaltyPoints, boolean isEqual) {
        StandardItem item1 = new StandardItem(
                id, name, price, category, loyaltyPoints);
        StandardItem item2 = new StandardItem(
                1, "Item1", 10.0f, "Category1", 10);
        assertEquals(isEqual, item1.equals(item2));
    }

//    @Test
//    public void testEquals_compareDifferentObjects_False() {
//        StandardItem item1 = new StandardItem(
//                1, "Item1", 10.0f, "Category1", 10);
//        StandardItem item2 = new StandardItem(
//                2, "Item2", 10.0f, "Category2", 20);
//        assertNotEquals(item1, item2);
//    }

    @Test
    @DisplayName("Test Copy:")
    public void testCopy_compareCopiedAndOriginalItem_True() {
        StandardItem original = new StandardItem(1, "Item", 10.0f, "Category", 5);
        StandardItem copy = original.copy();
        assertNotSame(original, copy);
        assertEquals(original, copy);
    }
}
