package cz.cvut.fel.ts1.hw02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    private Calculator calc;

    @BeforeEach
    public void initCalc() {
        calc = new Calculator();
    }

    @Test
    void add_addsOneAndTwo_3() {
        int result = calc.add(1, 2);
        Assertions.assertEquals(3, result);
    }

    @Test
    void subtract_subtractsOneFromTwo_1() {
        int result = calc.subtract(2, 1);
        Assertions.assertEquals(1, result);
    }

    @Test
    void multiply_multipliesTwoAndThree_6() {
        int result = calc.multiply(2,3);
        Assertions.assertEquals(6, result);
    }

    @Test
    void divide_dividesSixByZero_errorMessage() {
        int a = 6;
        int b = 0;
        String exceptionMessage = "Nelze delit nulou";

        Exception exception = assertThrows(Exception.class, () -> calc.divide(a, b));
        Assertions.assertEquals(exception.getMessage(), exceptionMessage);
    }


//    @Test
//    void divide_dividesSixByThree_2() {
//        try {
//            int result = calc.divide(6, 3);
//            assertEquals(2, result);
//        } catch (Exception e) {
//            String expectedMessage = "Nelze delit nulou";
//            String actualMessage = e.getMessage();
//            assertEquals(expectedMessage, actualMessage);
//        }
//    }
}