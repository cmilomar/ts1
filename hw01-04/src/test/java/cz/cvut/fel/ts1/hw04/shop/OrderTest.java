package cz.cvut.fel.ts1.hw04.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    private ShoppingCart shoppingCart;
    private StandardItem standardItem1, standardItem2;


    @BeforeEach
    public void initShoppingCart(){
        standardItem1 = new StandardItem(
                1, "Item1", 10.0f, "Category1", 10);
        standardItem2 = new StandardItem(
                2, "Item2", 20.0f, "Category2", 20);

        shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem1);
        shoppingCart.addItem(standardItem2);
    }

    @Test
    @DisplayName("Test Constructor:")
    public void testConstructor1_compareItemAttributes_attributesMatch(){
        Order order = new Order(shoppingCart, "Branibor", "FarFarAway", 10);

//        testing items
        ArrayList<Item> itemsFromShoppingCart = shoppingCart.getCartItems();
        ArrayList<Item> itemsFromOrder = order.getItems();
        assertEquals(itemsFromShoppingCart, itemsFromOrder);

//        testing customerName
        assertEquals("Branibor", order.getCustomerName());

//        testing customerAddress
        assertEquals("FarFarAway", order.getCustomerAddress());

//        testing state
        assertEquals(10, order.getState());
    }

    @Test
    @DisplayName("Test Constructor without 'state' argument:")
    public void testConstructor2_compareItemAttributes_attributesMatch(){
        Order order = new Order(shoppingCart, "Branibor", "FarFarAway");

//        testing items
        ArrayList<Item> itemsFromShoppingCart = shoppingCart.getCartItems();
        ArrayList<Item> itemsFromOrder = order.getItems();
        assertEquals(itemsFromShoppingCart, itemsFromOrder);

///        testing customerName
        assertEquals("Branibor", order.getCustomerName());

//        testing customerAddress
        assertEquals("FarFarAway", order.getCustomerAddress());

//        testing state
        assertEquals(0, order.getState());
    }

    @Test
    @DisplayName("Test Null values:")
    public void testNullValues_createOrderWithNullValues_NullPointerException(){
        assertThrows(NullPointerException.class, () -> {
            Order order = new Order(null, null, null, 0);
        });
    }

}