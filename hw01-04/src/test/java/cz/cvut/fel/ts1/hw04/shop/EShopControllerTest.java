package cz.cvut.fel.ts1.hw04.shop;

import cz.cvut.fel.ts1.hw04.archive.PurchasesArchive;
import cz.cvut.fel.ts1.hw04.storage.NoItemInStorage;
import cz.cvut.fel.ts1.hw04.storage.Storage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class EShopControllerTest {
    private Storage mockedStorage;
    private PurchasesArchive mockedArchive;
    private ShoppingCart shoppingCart;
    private ArrayList<Order> orders;
    private Item item1, item2;


    @BeforeEach
    public void initEShop_createsMockedStorageAndMockedArchive_EShopControllerIsSet(){
        mockedStorage = mock(Storage.class);
        mockedArchive = mock(PurchasesArchive.class);

        item1 = new StandardItem(1, "Item1", 100, "Category1", 10);
        item2 = new StandardItem(2, "Item2", 200, "Category2", 20);
        shoppingCart = new ShoppingCart();

        shoppingCart.addItem(item1);
        shoppingCart.addItem(item2);

        EShopController.setStorage(mockedStorage);
        EShopController.setArchive(mockedArchive);
//        EShopController.startEShop();

    }

    @Test
    @DisplayName("Test behaviour of PurchaseShoppingCart with mocked objects:")
    public void testPurchaseShoppingCart_callsProcessOrderAndPutOrderToArchive_methodsCalledVerification()
            throws NoItemInStorage {
        EShopController.purchaseShoppingCart(
                shoppingCart, "Jackie Chan", "77 RandomKarate St.");
        verify(mockedStorage).processOrder(any(Order.class));
        verify(mockedArchive).putOrderToPurchasesArchive(any(Order.class));
    }

    @Test
    @DisplayName("Test behaviour of PurchaseShoppingCart with empty ShoppingCart:")
    public void testPurchaseEmptyShoppingCart_createsEmptyShoppingCartAndCallsMethods_NoItemInStorageException()
            throws NoItemInStorage {
        shoppingCart = new ShoppingCart();
        doThrow(new NoItemInStorage()).when(mockedStorage).processOrder(any(Order.class));
        assertThrows(NoItemInStorage.class, () -> {
            EShopController.purchaseShoppingCart(
                    shoppingCart, "Jackie Chan", "77 RandomKarate St.");
        });
    }
}