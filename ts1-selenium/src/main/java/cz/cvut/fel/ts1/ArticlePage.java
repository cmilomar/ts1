package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ArticlePage {

    private WebDriver driver;

    @FindBy(css = "#main > section > div > div > div.app-article-masthead__info > h1")
    private WebElement titleElement;
    private String title;

    private WebElement textElement;
    private String text;

    public ArticlePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getTitle() {
        title = titleElement.getText();
        return title;
    }

    public String getAbstractText() {
        textElement = driver.findElement(By.xpath("//*[@id=\"Abs1-content\"]/p"));
        text = textElement.getText();
        return text;
    }
}
