package cz.cvut.fel.ts1;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static cz.cvut.fel.ts1.AdvancedSearchPage.ADVANCED_SEARCH_PAGE_URL;
import static cz.cvut.fel.ts1.SignupPage.SIGNUP_PAGE_URL;

public class HomePage {
    protected WebDriver driver;

    @FindBy(id = "identity-account-widget")
    private WebElement loginButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public LoginPage navigateToLoginPage() {
        loginButton.click();
        return new LoginPage(driver);
    }

    public SignupPage navigateToSignupPage() {
        driver.get(SIGNUP_PAGE_URL);
        return new SignupPage(driver);
    }
}
