package cz.cvut.fel.ts1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;

    @FindBy(id = "login-email")
    private WebElement emailInput;

    @FindBy(id = "login-password")
    private WebElement pwdInput;

    @FindBy (id = "email-submit")
    private WebElement submitEmailButton;

    @FindBy (id = "password-submit")
    private WebElement submitPwdButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public LoginPage login(String email, String password) {
        typeEmail(email);
        typePwd(password);
        return this;
    }

    public LoginPage typeEmail(String email) {
        emailInput.sendKeys(email);
        submitEmailButton.click();
        return this;
    }

    public LoginPage typePwd(String pwd){
        pwdInput.sendKeys(pwd);
        submitPwdButton.click();
        return this;
    }


//
//    public LoginPage submitLoginExpectingFailure() {
//        driver.findElement(loginButtonLocator).submit();
//        return new LoginPage(driver);
//    }


}
