package cz.cvut.fel.ts1;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AdvancedSearchPage {
    private WebDriver driver;
    public static final String ADVANCED_SEARCH_PAGE_URL = "https://link.springer.com/advanced-search";

    @FindBy (id = "all-words")
    private WebElement allWordsInput;

    @FindBy (id = "least-words")
    private WebElement leastWordsInput;

    @FindBy(id = "date-facet-mode")
    private WebElement dropdownYear;

    @FindBy(id = "facet-start-year")
    private WebElement startYear;

//    @FindBy(id = "submit-advanced-search")
    @FindBy(xpath = "//*[@id=\"submit-advanced-search\"]")
    private WebElement searchButton;

//    @FindBy (css = "#list-content-type-filter > li:nth-child(2) > div > label > span > span.app-search-filter__filter-name")
    private WebElement articleCheckbox;

    @FindBy(css = "[data-track-label='update results button']")
    private WebElement updateResultsButton;

    @FindBy(xpath = "//*[@id=\"exact-phrase\"]")
    private WebElement exactPhraseInput;

    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public AdvancedSearchPage search(String allWords, String leastWords, String dropdown, String year){
        typeAllWord(allWords);
        typeleastWords(leastWords);
        selectInYearOption(dropdown);
        typeYear(year);
        submitSearch();
        return this;
    }

    public AdvancedSearchPage typeAllWord(String words){
        allWordsInput.sendKeys(words);
        return this;
    }

    public AdvancedSearchPage typeExactPhrase(String phrase){
        exactPhraseInput.sendKeys(phrase);
        return this;
    }
    public AdvancedSearchPage typeleastWords(String words){
        leastWordsInput.sendKeys(words);
        return this;
    }

    public AdvancedSearchPage selectInYearOption(String byText) {
        dropdownYear = driver.findElement(By.id("date-facet-mode"));
//        WebElement clickableDropdown = wait.until(ExpectedConditions.elementToBeClickable(dropdown));
        Select dropdown = new Select(dropdownYear);
        dropdown.selectByVisibleText(byText);
        return this;
    }

    public AdvancedSearchPage typeYear(String year){
        startYear.sendKeys(year);
        return this;
    }

    public AdvancedSearchPage submitSearch(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
        return this;
    }
}
