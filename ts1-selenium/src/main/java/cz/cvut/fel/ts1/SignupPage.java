package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignupPage {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"login-email\"]")
    private WebElement emailInput;

    @FindBy(xpath = "//*[@id=\"email-submit\"]")
    private WebElement submitEmailButton;

    public static final String SIGNUP_PAGE_URL = "https://link.springer.com/signup-login";

    @FindBy(css = "#login-password")
    private WebElement loginPasswordInput;

    @FindBy (id = "registration-given-names")
    private WebElement givenNamesInput;

    @FindBy (id = "registration-family-name")
    private WebElement familyNameInput;

    @FindBy (id = "registration-password")
    private WebElement pwdInput;

    @FindBy (id = "registration-password-confirm")
    private WebElement repeatPwdInput;

    @FindBy (xpath = "//*[@id=\"form-registration\"]/fieldset/div[5]/div/div/label/span[1]")
    private WebElement TermsConditionsCheckbox;

    @FindBy (id = "form-registration-submit")
    private WebElement registrationButton;

    public SignupPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SignupPage signup(String email, String password, String givenName, String familyName) {
        typeEmail(email);
        if (emailExists()) {
            typeLoginPwd(password);
        } else {
            createAccount(givenName, familyName, password);
        }
        return this;
    }

    public SignupPage typeEmail(String email) {
        emailInput.sendKeys(email);
        submitEmailButton.click();
        return this;
    }

    public boolean emailExists(){
        try {
            driver.findElement(By.cssSelector("#content > h1"));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public SignupPage typeLoginPwd(String pwd) {
        loginPasswordInput.sendKeys(pwd);
        return this;
    }

    public SignupPage createAccount(String givenNames, String familyName, String pwd) {
        givenNamesInput.sendKeys(givenNames);
        familyNameInput.sendKeys(familyName);
        pwdInput.sendKeys(pwd);
        repeatPwdInput.sendKeys(pwd);
        TermsConditionsCheckbox.click();
        registrationButton.click();
        return this;
    }
}
