package cz.cvut.fel.ts1;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;

import static cz.cvut.fel.ts1.AdvancedSearchPage.ADVANCED_SEARCH_PAGE_URL;

public class Main7 {
    public static void main(String[] args) {

        //init
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        //homepage
        driver.get("https://link.springer.com/");
        cookieBanner(driver);
        HomePage homePage = new HomePage(driver);

        //signup process
        homePage.navigateToSignupPage();
        SignupPage signupPage = new SignupPage(driver);
        cookieBanner(driver);
        signupPage.signup("haze.canton_06@icloud.com", "password007", "James", "Bond");

        //login
//        homePage.navigateToLoginPage();
//        LoginPage loginPage = new LoginPage(driver);
//        loginPage.login("kumquat-voided.03@icloud.com", "vomtis-huqxo0-vudtIh");

        //advanced search
        driver.get(ADVANCED_SEARCH_PAGE_URL);
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);
        advancedSearchPage.search("Page Object Model", "Selenium Testing", "in", "2024");

        //search articles
        SearchPage searchPage = new SearchPage(driver);
        searchPage.searchArticles(4);

        //results of the search
        ArrayList<String> titles = searchPage.getTitles();
        ArrayList<String> links = searchPage.getLinks();
        ArrayList<String> dates = searchPage.getDates();
        ArrayList<String> texts = searchPage.getArticleAbstractText();
        for (int i = 0; i < 4; i++) {
            System.out.println("Article Title: " + titles.get(i));
            System.out.println("DOI: " + links.get(i));
            System.out.println("Publication Date: " + dates.get(i));
            System.out.println("Abstract Article: " + texts.get(i));
        }
        driver.quit();
    }

    public static void cookieBanner(WebDriver driver){
        WebElement cookieBanner = driver.findElement(By.className("cc-banner"));
        if (cookieBanner.isDisplayed()) {
            try{
                WebElement closeButton = driver.findElement(By.cssSelector("body > dialog > div.cc-banner__content > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-reject"));
                closeButton.click();
            } catch (NoSuchElementException | TimeoutException e){
                WebElement closeButton = driver.findElement(By.cssSelector("body > section > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-reject"));
                closeButton.click();
            }
        }
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.invisibilityOf(cookieBanner));
    }
}
