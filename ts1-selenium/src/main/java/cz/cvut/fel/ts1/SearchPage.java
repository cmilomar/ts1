package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class SearchPage {
    private WebDriver driver;
    public static final String SEARCH_PAGE_URL = "https://link.springer.com/search";

    @FindBy (id = "search-springerlink")
    private WebElement wordSearchInput;

    private WebElement articleCheckbox;

    @FindBy (id = "search-submit")
    private WebElement searchSubmitButton;

    @FindBy(css = "[data-track-label='update results button']")
    private WebElement updateResultsButton;

    private WebElement articleTitle;
    private WebElement articleDOI;
    private WebElement articleDate;
    private WebElement articleButton;

    public ArrayList<String> titles = new ArrayList<>();
    public ArrayList<String> links = new ArrayList<>();
    public ArrayList<String> dates = new ArrayList<>();
    public ArrayList<String> articleAbstractText = new ArrayList<>();

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SearchPage searchArticles(int numberOfArticles){
        searchTypeArticle();
        setArticleData(numberOfArticles);
        return this;
    }

//    public SearchPage searchWords(String words){
//        wordSearchInput.sendKeys(words);
//        searchSubmitButton.click();
//        return this;
//    }

    public SearchPage searchTypeArticle(){
        articleCheckbox = driver.findElement(By.xpath("//*[@id=\"list-content-type-filter\"]/li[1]/div/label/span/span[1]"));
//        articleCheckbox = driver.findElement(By.cssSelector("#list-content-type-filter > li:nth-child(2) > div > label"));
        articleCheckbox.click();
        updateResultsButton.click();
        return this;
    }

    public void setArticleData(int numberOfArticles){

        for (int i = 1; i <= numberOfArticles; i++) {
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/h3")));

            articleTitle = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/h3"));
            articleDOI = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div[2]/ol/li["+ i +"]/div[1]/div/h3/a"));
//            articleDate = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div[2]/ol/li["+ i +"]/div[1]/div/div[3]/div/span[3]"));

            String title = articleTitle.getText();
            String doi = articleDOI.getAttribute("href");
//            String date = articleDate.getText();

            titles.add(title);
            links.add(doi);
//            dates.add(date);

            ArticlePage articlePage = goToArticlePage(i);

            //store info about title and abstract text into array
            String abstractText = articlePage.getAbstractText();
            articleAbstractText.add(abstractText);
            articleDate = driver.findElement(By.cssSelector("time"));
            String date = articleDate.getText();
            dates.add(date);

            //go back to search page
            driver.navigate().back();
        }
    }

    public ArticlePage goToArticlePage(int articleNumber){
        articleButton = driver.findElement(By.cssSelector("#main > div > div:nth-child(4) > div > div:nth-child(2) > div:nth-child(2) > ol > li:nth-child("+ articleNumber +") > div.app-card-open__main > div > h3 > a"));
        articleButton.click();
        return new ArticlePage(driver);
    }

    public ArrayList<String> getArticleAbstractText() {
        return articleAbstractText;
    }

    public ArrayList<String> getTitles() {
        return titles;
    }

    public ArrayList<String> getLinks() {
        return links;
    }

    public ArrayList<String> getDates() {
        return dates;
    }
}
