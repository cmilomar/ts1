package cz.cvut.fel.ts1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import static cz.cvut.fel.ts1.AdvancedSearchPage.ADVANCED_SEARCH_PAGE_URL;

class SpringerTest {
    private WebDriver driver;
    private LoginPage loginPage;
    private SearchPage searchPage;
    private AdvancedSearchPage advancedSearchPage;
    private ArticlePage articlePage;

    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        // Inicializace Page Object Modelů
        loginPage = new LoginPage(driver);
        searchPage = new SearchPage(driver);
        advancedSearchPage = new AdvancedSearchPage(driver);
        articlePage = new ArticlePage(driver);

        // Otevření domovské stránky
        driver.get("https://link.springer.com/signup-login");

        // Přihlášení
        loginPage.login("kumquat-voided.03@icloud.com", "vomtis-huqxo0-vudtIh");
        driver.get(ADVANCED_SEARCH_PAGE_URL);
        cookieBanner(driver);

    }

    @AfterEach
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @ParameterizedTest
    @CsvSource ({
            "A detector for page-level handwritten music object recognition based on deep learning, A detector for page-level handwritten music object, https://link.springer.com/article/10.1007/s00521-023-08216-6, 20 January 2023",
            "Model-based testing leveraged for automated web tests, Model-based testing leveraged, https://link.springer.com/article/10.1007/s11219-021-09575-w, 27 November 2021",
            "SleepReplacer: a novel tool-based approach for replacing thread sleeps in selenium WebDriver test code, a novel tool-based approach for replacing thread sleeps, https://link.springer.com/article/10.1007/s11219-022-09596-z, 12 August 2022",
            "A brief review of state-of-the-art object detectors on benchmark document images datasets, brief review of state-of-the-art object detectors, https://link.springer.com/article/10.1007/s10032-023-00431-0, 25 April 2023"
    })
    public void testSpringerSearch(String title, String exactPhrase, String expectedLink, String expectedDate) {
        // Pokročilé vyhledávání
        advancedSearchPage.typeExactPhrase(exactPhrase);
        advancedSearchPage.submitSearch();
        searchPage.searchArticles(1);
        ArrayList<String> resultTitle = searchPage.getTitles();
        ArrayList<String> resultLink = searchPage.getLinks();
        ArrayList<String> resultDate = searchPage.getDates();

        // Procházení výsledků vyhledávání
        ArrayList<String> titles = searchPage.getTitles();
        ArrayList<String> links = searchPage.getLinks();
        ArrayList<String> dates = searchPage.getDates();

        assertEquals(title, titles.getFirst());
        assertEquals(expectedLink, links.getFirst());
        assertEquals(expectedDate, dates.getFirst());

    }
    public static void cookieBanner(WebDriver driver){
        WebElement cookieBanner = driver.findElement(By.className("cc-banner"));
        if (cookieBanner.isDisplayed()) {
            try{
                WebElement closeButton = driver.findElement(By.cssSelector("body > dialog > div.cc-banner__content > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-reject"));
                closeButton.click();
            } catch (NoSuchElementException | TimeoutException e){
                WebElement closeButton = driver.findElement(By.cssSelector("body > section > div > div.cc-banner__footer > button.cc-button.cc-button--contrast.cc-banner__button.cc-banner__button-accept"));
                closeButton.click();
            }
        }
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.invisibilityOf(cookieBanner));
    }
}